import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class DataService {

    constructor(private readonly http: HttpClient) { }

    getData(): Observable<any> {
        return this.http.get('https://jsonplaceholder.typicode.com/posts');
    }
    signUp(user: any): Observable<any> {


        return this.http.post(user, 'http://localhost:5000/skicco/userSignup', {})

    }
}
